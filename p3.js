let teams= getTeams()

//function qui récupère dans le tableau géré par la fonction choose les informations de chaque pokémons de l'équipe
function PrintPokemonTeam(team, tag) {
    var str = ''
    for (let i = 0; i <= 5; i++) {
        str += printPokemon(team[i])
    }
    document.getElementById(tag).innerHTML = str
}

//function permettant l'affichage en html les pokemons, leur image etleur force sur la page 3
function printPokemon(pokemon) {
    return `
    <div class="pokemon-card ${pokemon.alive ? 'alive' : 'dead'}">
        <img class="img-team" src="/media/${pokemon.nom}.png"/>
        <div class="name"> ${pokemon.nom} </div>
        <div class="force"> ${pokemon.force} </div>
    </div>
    `
}

//sert a lancer la fonction init au lancement de la page
window.addEventListener('DOMContentLoaded', init);


//lance la fonction printPokemonTeam
function init() {
    console.log(teams)
    PrintPokemonTeam(teams.team1, "team-1")
    PrintPokemonTeam(teams.team2, "team-2")
}


function goArene(){
    window.location = 'http://127.0.0.1:5500/page4.html'
}
