let teams= getTeams()


for (let i = 0; i < teams.team1.length; i++) {
    pk = teams.team1[i];
    pk = Object.assign(new Pokemon, pk)
    teams.team1[i] = pk;
}
for (let i = 0; i < teams.team2.length; i++) {
    pk = teams.team2[i];
    pk = Object.assign(new Pokemon, pk)
    teams.team2[i] = pk;
}


//function permetant a la fois d'aller à la page suivante et de selectionner au hasard un pokemon dans chaque équipe
function selectPoke(team) {
    do{
        let index = Math.floor(Math.random() * team.length);
        pkm = team[index]
    }while(pkm.alive != true)
    return pkm;
}

let pkm1 = selectPoke(teams.team1)
let pkm2 = selectPoke(teams.team2)


//fonction de combat
function combat() {
    console.log(pkm1)
    console.log(pkm2)
    // console.log("combat commence entre ", pkm1, pkm2);
    pkmWin = pkm1.attack(pkm2);
    pkmLoose = pkm1.nom == pkmWin.nom ? pkm2 : pkm1;
    setPkmWin(pkmWin)
    /*
    console.log('pkmLoose :'+pkmWin.nom);
    console.log(teams);
    */
    for(let i =0;i < teams[pkmLoose.team].length;i++){
        if(teams[pkmLoose.team][i].nom == pkmLoose.nom){
            teams[pkmLoose.team][i].alive = false;
        }
    }
    setTeams(teams)
    
    console.log(teams);
    console.log(pkm1)
    console.log(pkm2)

    //document.cookie = 

    // if (pkm1.evolution()) {
    //     console.log("evolution Y");
    //     Evoli1 = new Noctali('Younes');
    // }

    // if (pkm2.evolution()) {
    //     console.log("evolution R");
    //     Pikachu1 = new Raichu('Rudy');
    // }
}

//function permettant l'affichage en html les pokemons, leur image etleur force sur la page 3
function printPokemon(pkm, tag) {
    var str = ''
    str += print1Pokemon(pkm)
    document.getElementById(tag).innerHTML = str
}

function print1Pokemon(pkm){
    return `
    <div class="pokemon-card">
        <div class="name"> ${pkm.nom} </div>
        <div class="force"> ${pkm.force} </div>
        <div class="img-pkm">
            <img class="img-team" src="/media/${pkm.nom}.png"/>
        </div>
    </div>`
}

function affichage() {
    printPokemon(pkm1, "team-1")
    printPokemon(pkm2, "team-2")
}

function goWin() {
    window.location = 'http://127.0.0.1:5500/page5.html'
}

//sert a lancer la fonction init au lancement de la page
window.addEventListener('DOMContentLoaded', affichage);


//déplacer les pokemons
window.addEventListener('DOMContentLoaded', init);

function init(){
    var boxOne = document.getElementsByClassName('img-team')[0];
    var boxTwo = document.getElementsByClassName('img-team')[1];
    document.getElementsByClassName('fight')[0].onclick = function() {
        document.getElementById("fight").disabled = true;
        if(this.innerHTML === 'Combat')
        { 
          boxOne.classList.add('horizTranslate');
          boxTwo.classList.add('horizTranslate');
          combat();
          
          setTimeout(
              function(){
                //   goWin();
                  checkVictory()
                },3500
            );
          
        } else {
          this.innerHTML = 'Combat';
          var computedStyle = window.getComputedStyle(boxOne),
              marginLeft = computedStyle.getPropertyValue('margin-left');
          boxOne.style.marginLeft = marginLeft;
          boxOne.classList.remove('horizTranslate');    
        }  
    }       
}

// return true if team has loosed else false
function isTeamLoosing(team){
    for (let index = 0; index < team.length; index++) {
        const pokemon = team[index];
        if(pokemon.alive)
            return false;
    }
    return true;
}

function checkVictory(){
    // si y'a victoire p6
    teams.team1
    if ( isTeamLoosing(teams.team1) || isTeamLoosing(teams.team2) ){
        window.location = 'http://127.0.0.1:5500/page6.html'
    // va page 3
    }else{
        goWin()
    }
}