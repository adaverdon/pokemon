//SETUP
class Pokemon {
    nom;
    force;
    exp;
    maxExp = 100;
    team;
    alive = true;

    constructor(nom, force) {
        this.exp = 0;
        this.nom = nom;
        this.force = force;
    }

    attack(pokemonCible) {
        if (this.force >= pokemonCible.force) {
            this.win()
            return this;
            // pokemonCible.loose()
            // console.log("win the fight !")
            // console.log(Evoli1)
            // console.log(Pikachu1)
            // return this + "win the fight !"
        } else {
            // this.loose()
            pokemonCible.win()
            return pokemonCible;
            // console.log("loose the fight !")
            // console.log(Evoli1)
            // console.log(Pikachu1)
        }
    }
    win() {
        this.force += 5;
        // this.evolution()
        // pokemonCible.evolution()
    }
    // loose() {
    //     this.exp += 5;
    //     // this.evolution()
    //     // pokemonCible.evolution()
    // }

    // evolution() {
    //     console.log(this.proprio, this.exp);
    //     if (this.exp >= this.maxExp) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

//     heritage(enfant) {
//         console.log("parent");
//         enfant();
//     }
// }

// class Pikachu extends Pokemon {
//     constructor(prenom) {
//         super(prenom)
//     }
//     force = 40;

//     heritage() {
//         super.heritage(() => {
//             console.log("enfant pikachu")
//         })
//     }
// }

// class Evoli extends Pokemon {
//     constructor(prenom) {
//         super(prenom)
//     }
//     force = 45;
//     heritage() {
//         super.heritage(() => {
//             console.log("enfant evolie")
//         })
//     }
// }

// class Raichu extends Pokemon {
//     constructor(prénom) {
//         super(prénom)
//     }
//     force = 80
// }

// class Noctali extends Evoli {
//     constructor(prénom) {
//         super(prénom)
//     }
//     force = 90
}

/* GAME */
let Pokemons = []

//remplissage du tableau de pokemon
Pokemons.push(new Pokemon('Pikachu', 60 ))
Pokemons.push(new Pokemon('Evoli', 70))
Pokemons.push(new Pokemon('Miaouss', 60))
Pokemons.push(new Pokemon('Poussifeu', 60))
Pokemons.push(new Pokemon('Brutalibré', 90))
Pokemons.push(new Pokemon('Pachirisu', 70))
Pokemons.push(new Pokemon('Carapuce', 60))
Pokemons.push(new Pokemon('Bulbizarre', 70 ))
Pokemons.push(new Pokemon('Lixy', 60))
Pokemons.push(new Pokemon('Draco', 90))
Pokemons.push(new Pokemon('Mysdibule', 90))
Pokemons.push(new Pokemon('Metalosse', 150))
Pokemons.push(new Pokemon('Togedemaru', 80))
Pokemons.push(new Pokemon('Scarhino', 120))
Pokemons.push(new Pokemon('Motisma', 80))
Pokemons.push(new Pokemon('Leviator', 240))
Pokemons.push(new Pokemon('Lucario', 140))
Pokemons.push(new Pokemon('Dracaufeu', 170))
Pokemons.push(new Pokemon('Tortank', 180))
Pokemons.push(new Pokemon('Aspicot', 40))

//export default Pokemons;
function setVarInCookie(varname,value){
    let cookie = JSON.parse(document.cookie);
    cookie[varname] = value;
    document.cookie = JSON.stringify(cookie);
}

function setTeams(teams){
    setVarInCookie('teams',teams);
}

function setPkmWin(pkm){
    setVarInCookie('pkmWin',pkm);
}


function getTeams(){
    return JSON.parse(document.cookie)['teams']
}

function getPkmWin(){
    return JSON.parse(document.cookie)['pkmWin']
}